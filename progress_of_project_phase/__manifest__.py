# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Progress of project phase',
    'version': '1.1',
    'website': '',
    
    "price": 20.00,
    "currency": 'USD',
    'license': 'OPL-1',
    'author': 'Tb25',
    'email': 'torbatj79@gmail.com',
    
    'category': 'Project',
    'sequence': 1,
    'summary': 'You can see Progress of project phase on Project form. Progress of phase. Percent of phase. Phase percent. Phase project. Percent of project. Percent by phase. Phase percent. Phase of project. Phase performance. Performance phase. Progress of phase. Progress phase. Phase progress. Performance progress',
    'depends': [
        'project',
        'bi_odoo_project_phases',
        'project_timesheet_holidays',
    ],
    'description': "Percent of each phase",
    'data': [
        'wizard/p3_view.xml',
        'security/ir.model.access.csv',
    ],
    'js': [
        
           ],
    'images': [
        'static/src/img/banner.png'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
