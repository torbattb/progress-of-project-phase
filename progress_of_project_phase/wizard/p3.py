from odoo import api, fields, models, tools, SUPERUSER_ID, _

class ProjectPhaseProgress(models.Model):
    _name = 'project.phase.progress'
    phase_id = fields.Many2one('project.task.phase', 'Phase')
    progress = fields.Float(string='Progress (%)')
    project_id = fields.Many2one('project.project', 'Project')

class project_project(models.Model):
    _inherit = 'project.project'
    phase_progress_id = fields.One2many('project.phase.progress', 'project_id', 'Phase progress')

class task(models.Model):
    _name = 'project.task'
    _inherit = 'project.task'
    @api.depends('effective_hours', 'subtask_effective_hours', 'planned_hours')
    def _compute_progress_hours(self):
        for task in self:
            if (task.planned_hours > 0.0):
                task_total_hours = task.effective_hours + task.subtask_effective_hours
                if task_total_hours > task.planned_hours:
                    task.progress = 100
                else:
                    task.progress = round(100.0 * task_total_hours / task.planned_hours, 2)
            else:
                task.progress = 0.0
            task_phases = self.env['project.task'].search([('project_id', '=', task.project_id.id),('phase_id', '=', task.phase_id.id)])
            if task_phases:
                planned_sum = 0.0
                effective_sum = 0.0
                phase_progress = 0.0
                for line in task_phases:
                    planned_sum += line.planned_hours
                    effective_sum += line.effective_hours
                phase_progress = (effective_sum)*100.0/planned_sum
                p3_obj_id = self.env['project.phase.progress'].search([('project_id', '=', task.project_id.id),('phase_id', '=', task.phase_id.id)])
                if p3_obj_id:
                    p3_obj_id.progress = phase_progress
                else:
                    self.env['project.phase.progress'].create({'phase_id':task.phase_id.id,
                                                              'progress':phase_progress,
                                                              'project_id':task.project_id.id,})